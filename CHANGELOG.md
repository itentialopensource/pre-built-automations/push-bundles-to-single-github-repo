
## 0.0.10 [06-29-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/push-bundles-to-single-github-repo!9

---

## 0.0.9 [05-11-2022]

* certified for 2021.2

See merge request itentialopensource/pre-built-automations/push-bundles-to-single-github-repo!8

---

## 0.0.8 [04-21-2022]

* Patch/dsup 1337

See merge request itentialopensource/pre-built-automations/push-bundles-to-single-github-repo!7

---

## 0.0.5-2021.1.1 [03-23-2022]

* Patch/dsup 1324

See merge request itentialopensource/pre-built-automations/push-bundles-to-single-github-repo!4

---

## 0.0.5-2021.1.0 [10-18-2021]

* Updated for 2021.1

See merge request itentialopensource/pre-built-automations/push-bundles-to-single-github-repo!2

---

## 0.0.5 [10-18-2021]

* Updated for 2021.1

See merge request itentialopensource/pre-built-automations/push-bundles-to-single-github-repo!2

---

## 0.0.4 [10-14-2021]

* Patch/dsup 1053

See merge request itentialopensource/pre-built-automations/push-bundles-to-single-github-repo!1

---

## 0.0.3 [10-11-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [10-08-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---
